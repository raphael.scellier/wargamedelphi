unit DeckClass;

interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CardClass, System.Generics.Collections;

type
  TDeck = class (TObject)
  private
    cards_list : TList<TCard>;
    cards_amount : integer;
  public
    constructor Create();
    function cardsAmount():integer;
    function cardRemove(carte : TCard);
    function cardAdd(carte : TCard);
    function getDeck():TList<TCard>;
    function shuffle();
    function getRandomCard():TCard;
  end;

implementation
constructor TDeck.Create();
begin
  cards_list := TList<TCard>.Create;
  cards_amount := 0;
end;
function TDeck.cardsAmount():integer;
begin
  Result := cards_amount;
end;
function TDeck.cardRemove(carte : TCard);
begin
end;
function TDeck.cardAdd(carte : TCard);
begin

end;



end.

