unit CardClass;

interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs;

type
  TCard = class (TObject)
  private
    value : integer;
    color : integer;
  public
    constructor Create(val,col : integer);
    function get_value():integer;
    function get_color():integer;
    function set_value(val:integer);
    function set_color(col:integer);
  end;

implementation
constructor TCard.Create(val,col : integer);
begin
  value := val;
  color := col;
end;
function TCard.get_value;
begin
  Result := value;
end;
function TCard.get_color;
begin
  Result := color;
end;
function TCard.set_value(val: Integer);
begin
  value := val;
end;
function TCard.set_color(col: Integer);
begin
  color := col;
end;
end.
