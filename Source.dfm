object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 431
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 200
    Width = 32
    Height = 13
    Caption = 'Deck 1'
  end
  object Label2: TLabel
    Left = 512
    Top = 200
    Width = 32
    Height = 13
    Caption = 'Deck 2'
  end
  object deckGauche: TPanel
    Left = 48
    Top = 232
    Width = 97
    Height = 145
    TabOrder = 0
    object Label4: TLabel
      Left = 16
      Top = 128
      Width = 31
      Height = 13
      Caption = 'Label4'
    end
  end
  object deckDroit: TPanel
    Left = 496
    Top = 232
    Width = 97
    Height = 145
    TabOrder = 1
    object Label5: TLabel
      Left = 16
      Top = 128
      Width = 31
      Height = 13
      Caption = 'Label5'
    end
  end
  object jeuGauche: TPanel
    Left = 192
    Top = 232
    Width = 97
    Height = 145
    TabOrder = 2
    object Label6: TLabel
      Left = 16
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Label6'
    end
    object Label7: TLabel
      Left = 16
      Top = 35
      Width = 31
      Height = 13
      Caption = 'Label7'
    end
  end
  object jeuDroit: TPanel
    Left = 352
    Top = 232
    Width = 97
    Height = 145
    TabOrder = 3
    object Label8: TLabel
      Left = 16
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Label8'
    end
    object Label9: TLabel
      Left = 16
      Top = 35
      Width = 31
      Height = 13
      Caption = 'Label9'
    end
  end
  object deckReserve: TPanel
    Left = 272
    Top = 28
    Width = 97
    Height = 145
    TabOrder = 4
    object Label3: TLabel
      Left = 24
      Top = 66
      Width = 49
      Height = 13
      Caption = 'Label3'
    end
  end
end
